![Mom Goggle Icon](https://gitlab.com/austinbagley/mom-goggles/raw/master/icon128.png)

# Mom Goggles Chrome Extension

Chrome extension to make PS Skills app more demo-friendly by modifying distracting UX elements.

## Changelog

### Version 0.1.1

- Removed skill assessment re-take prompt
- Modified learner home header background color to grey

## Installation

1. Download latest [here](https://gitlab.com/austinbagley/mom-goggles/-/archive/master/mom-goggles-master.zip)
2. Unzip folder
3. Type chrome://extensions in the address bar or go to menu > more tools > extensions
4. Turn the "Developer mode" switch on, and click "LOAD UNPACKED" ![chrome extension install window screenshot](https://developer.chrome.com/static/images/get_started/load_extension.png)
5. Select the mom-goggles folder and hit 'Select'
6. Navigate to the skills app and reload the page

## Contributor Resources

[Google Extension Guide](https://developer.chrome.com/extensions/devguide)
[Simple Extension Tutorial](https://blog.lateral.io/2016/04/create-chrome-extension-modify-websites-html-css/)
